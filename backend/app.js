const { PORT_NUMBER } = require("./settings"),
  express = require("express"),
  chalk = require("chalk"),
  centralCommand = require("./centralCommandModel"),
  app = express();

  app.use((req,res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");// TODO: make explict
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })

app.get("/", (req, res) => res.send("yo"));
app.get("/:id", (req, res) => centralCommand.getHero(req, res));
app.get("/:id/appearance", (req, res) => centralCommand.getHeroApperance(req, res));
app.get("/:id/biography", (req, res) => centralCommand.getHeroBio(req, res));
app.get("/:id/connections", (req, res) => centralCommand.getHeroConections(req, res));
app.get("/:id/image", (req, res) => centralCommand.getHeroImage(req, res));
app.get("/:id/powerstats", (req, res) => centralCommand.getHeroStats(req, res));
app.get("/:id/work", (req, res) => centralCommand.getHeroWork(req, res));
app.get("/search/:name", (req, res) => centralCommand.getHeroByName(req, res));
app.listen(PORT_NUMBER, () => console.log(`Server up and running on port ${chalk.blue(PORT_NUMBER)}`));
