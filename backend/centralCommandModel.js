const CacheService = require("./cache"),
  SUPERHERO_BASE_URI = "http://superheroapi.com/api",
  { DEFAULT_TTL, ACCESS_TOKEN } = require("./settings"),
  SUPERHERO_URI = `${SUPERHERO_BASE_URI}/${ACCESS_TOKEN}`,
  idCache = new CacheService(DEFAULT_TTL),
  nameCache = new CacheService(DEFAULT_TTL),
  axios = require("axios"),
  // FIXME: Review the caching tip
  paramValidation = ({ params }, res, type = "id") => !params && res.status(406).send(`missing required hero ${type}`),
  CentralCommandModel = {
    getHero(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            res.status(result.status).json(result.data);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },

    getHeroStats(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            console.info(this.getHero.name + " returns`", result.data);
            res.status(result.status).json(result.data.powerstats);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },
    getHeroBio(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            res.status(result.status).json(result.data.biography);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },
    getHeroApperance(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            res.status(result.status).json(result.data.appearance);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },
    getHeroWork(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            res.status(result.status).json(result.data.work);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },
    getHeroConections(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            res.status(result.status).json(result.data.connections);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },
    getHeroImage(req, res, next) {
      paramValidation(req, res);
      const { id } = req.params;
      return idCache.get(`heroId_${id}`, () =>
        axios
          .get(`${SUPERHERO_URI}/${id}`)
          .then(result => {
            res.status(result.status).json(result.data.image.url);
          })
          .catch(err => {
            console.error(err);
            throw errr;
          })
      );
    },
    getHeroByName(req, res, next) {
      paramValidation(req, res, "name");
      const { name } = req.params;
      return axios
        .get(`${SUPERHERO_URI}/search/${name}`)
        .then(result => {
          // console.info(this.getHeroByName.name, { name, result: result.data });
          //TODO: filter here and add set
          //   cache.set.'
          //  add name [{id:Name},{ id:Name} , {id:Name} ]
          if (result.data === "") return res.status(204);
          if(result.data.error ) return res.status(404).send(result.data.error)
          return res.status(result.status).send(
            result.data.results.map(({ id, name, image, biography }) => ({
              id,
              name,
              image: image.url,
              alignment: biography.alignment
            }))
          );
        })
        .catch(err => {
          console.error(err);
          throw errr;
        });
    }
  };

module.exports = CentralCommandModel;
