export const isEmpty = value =>
  value === null ||
  value === undefined ||
  value === "-" ||
  value === "No alter egos found." ||
  value === "null" ||
  (Array.isArray(value) && value.length === 0) ||
  Object.keys(value).length === 0;
