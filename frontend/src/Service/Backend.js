class Service {
  constructor() {
    //TODO: Replace with env variable
    this.BASE_URI = "http://localhost:8030/";
    console.debug(`${this.BASE_URI}`);
  }
  responseHandler = result => {
    console.info(this.responseHandler.name, result);
    if (result.response === "error") throw new Error(result.error);
    return result;
  };

  getHeroByName = name =>
    fetch(`${this.BASE_URI}search/${name}`)
      .then(res => res.json())
      .catch(error => {
        console.error(`${this.getHeroByName.name} failed`, error);
      });
  getCharacterById = id =>
    fetch(`${this.BASE_URI}${id}`)
      .then(res => res.json())
      .then(this.responseHandler)
      .catch(error => {
        console.error(`${this.getCharacterById.name} erorred`, error);
      });
  getHeroStats = id =>
    fetch(`${this.BASE_URI}${id}/powerstats`)
      .then(res => {
        console.debug(res.json());
        return res.json();
      })
      .catch(error => {
        console.error(`${this.getHeroStats.name} erorred`, error);
      });
  getHeroBio = id =>
    fetch(`${this.BASE_URI}${id}/biography`)
      .then(res => {
        console.debug(res.json());
        return res.json();
      })
      .catch(error => {
        console.error(`${this.getHeroBio.name} erorred`, error);
      });
  getHeroApperance = id =>
    fetch(`${this.BASE_URI}${id}/appearance`)
      .then(res => {
        console.debug(res.json());
        return res.json();
      })
      .catch(error => {
        console.error(`${this.getHeroApperance.name} erorred`, error);
      });
  getHeroWork = id =>
    fetch(`${this.BASE_URI}${id}/work`)
      .then(res => {
        console.debug(res.json());
        return res.json();
      })
      .catch(error => {
        console.error(`${this.getHeroWork.name} erorred`, error);
      });
  getHeroConnections = id =>
    fetch(`${this.BASE_URI}${id}/connections`)
      .then(res => {
        console.debug(res.json());
        return res.json();
      })
      .catch(error => {
        console.error(`${this.getHeroConnections.name} erorred`, error);
      });
  getHeroImage = id =>
    fetch(`${this.BASE_URI}${id}/image`)
      .then(res => {
        console.debug(res.json());
        return res.json();
      })
      .catch(error => {
        console.error(`${this.getHeroImage.name} erorred`, error);
      });
}

const ServiceLayer = new Service();
export default ServiceLayer;
