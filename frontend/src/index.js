import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "reakit";
import theme from "reakit-theme-default";
import MainLayout from "./components/Layout/Main";
import "./App.css"
const App = () => <Provider theme={theme}><MainLayout /></Provider> 


ReactDOM.render(<App />, document.getElementById("root"));
