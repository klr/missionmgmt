import React, { Fragment } from "react";
import { List, Flex, Heading, styled } from "reakit";
import { isEmpty } from "../../utils/helpers";

const StatsList = styled(List)`
  list-style-type: none;
  margin: 0;
  li {
    float: left;
    display: flex;
    flex-direction: column;
    align-content: "space-around";
    padding: 0.25em;
    font-weight: lighter;
    font-size: smaller;
    text-align: center;
  }
  li > b {
    text-transform: uppercase;
    display: block;
    padding: 0.25em;
    font-weight: bold;
    font-size: initial;
  }
`;

const Stats = ({ intelligence, strength, speed, durability, power, combat }) => (
  <Flex flexDirection="column">
    <Heading display="inline-block" as="h6">
      Powerstats
    </Heading>
    <StatsList>
      {!isEmpty(intelligence) && (
        <li>
          intelligence
          <b>{intelligence}</b>
        </li>
      )}
      {!isEmpty(strength) && (
        <li>
          strength
          <b>{strength}</b>
        </li>
      )}
      {!isEmpty(speed) && (
        <li>
          speed
          <b>{speed}</b>
        </li>
      )}
      {!isEmpty(durability) && (
        <li>
          durability
          <b>{durability}</b>
        </li>
      )}
      {!isEmpty(power) && (
        <li>
          power
          <b>{power}</b>
        </li>
      )}
      {!isEmpty(combat) && (
        <li>
          combat
          <b>{combat}</b>
        </li>
      )}
    </StatsList>
  </Flex>
);
export default Stats;
