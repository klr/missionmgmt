import React, { Fragment } from "react";
import PropTypes from "prop-types";

import { List, Flex, Heading, styled } from "reakit";
import { isEmpty } from "../../utils/helpers";

const ListItem = styled(List)`
  padding: 0.25em;
  li {
    text-transform: uppercase;
    text-transform: uppercase;
    font-weight: lighter;
    font-size: smaller;
  }
  li > b {
    text-transform: capitalize;
    padding-left: 0.65em;
    font-size: initial;
  }
`;

const Work = ({ occupation, base }) => (
  <Flex flexDirection="column">
    <Heading display="inline-block" as="h6">
      Work
    </Heading>
    <ListItem>
      {!isEmpty(occupation) && (
        <li>
          occupation <b>{occupation}</b>
        </li>
      )}
      {!isEmpty(base) && (
        <li>  
          HQ <b>{base}</b>
        </li>
      )}
    </ListItem>
  </Flex>
);
export default Work;
