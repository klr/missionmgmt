import React, { Fragment } from "react";
import PropTypes from "prop-types";

import { List, Flex, Heading, styled } from "reakit";
import { isEmpty } from "../../utils/helpers";

const ApperanceList = styled(List)`
  padding: 0.25em;
  text-transform: uppercase;
  font-weight: lighter;
  font-size: smaller;
  li {
    float: left;
    display: flex;
    flex-direction: column;
    align-content: "space-around";
    text-transform: uppercase;
    font-weight: lighter;
    font-size: smaller;
    align-content: center;
    padding: 0 0.25em;
    text-align: center;
  }
  li > b {
    text-transform: capitalize;
    padding-left: 0.65em;
    font-size: initial;
  }
`;
const MetricList = styled(List)`
  float: left;
  display: flex;
  flex-direction: column;
  align-content: "space-around";
  padding: 0.25em;
  text-transform: uppercase;
  font-weight: lighter;
  font-size: 11.1167px;
  align-content: center;
  padding: 0 0.25em;
  text-align: center;
  li {
    display: flex;
    flex-direction: column;
    text-align: center;
    text-transform: capitalize;
    padding-left: 0.65em;
    font-size: initial;
  }
`;

const Appearance = ({ gender, race, height, weight, "eye-color": eye, "hair-color": hair }) => (
  <Flex flexDirection="column">
    <Heading display="inline-block" as="h6">
      Appearance
    </Heading>
    <Flex flexDirection="row">
      <ApperanceList>
        {!isEmpty(gender) && (
          <li>
            gender
            <b>{gender}</b>
          </li>
        )}
        {!isEmpty(race) && (
          <li>
            race
            <b>{race}</b>
          </li>
        )}
        {!isEmpty(eye) && (
          <li>
            eyes
            <b>{eye}</b>
          </li>
        )}
        {!isEmpty(hair) && (
          <li>
            hair
            <b>{hair}</b>
          </li>
        )}
      </ApperanceList>
      {!isEmpty(height) && (
        <MetricList>
          height
          <li>{height.map(metric => !isEmpty(metric) && <b>{metric}</b>)}</li>
        </MetricList>
      )}
      {!isEmpty(weight) && (
        <MetricList>
          weight
          <li>{weight.map(metric => !isEmpty(metric) && <b>{metric}</b>)}</li>
        </MetricList>
      )}
    </Flex>
  </Flex>
);
export default Appearance;
