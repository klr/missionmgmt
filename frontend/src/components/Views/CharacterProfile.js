import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Block, Image, Heading, List, Grid } from "reakit";
import ServiceLayer from "../../Service/Backend";

import Stats from "./Stats";
import Bio from "./Bio";
import Work from "./Work";
import Connections from "./Connections";
import Appearance from "./Apperance";

class CharaterProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { profile: null };
  }
  componentWillMount() {
    ServiceLayer.getCharacterById(this.props.id)
      .then(results => this.setState(() => ({ profile: results })))
      .catch(error => {
        console.error(CharaterProfile.name, error);
      });
  }

  render = () => {
    return (
      this.state.profile && (
        <Grid.Item padding="1em">
          {this.state.profile.image && <Image maxWidth="25%" height="auto" src={this.state.profile.image.url} />}
          <Block width={this.state.profile.image ? "75%" : "100%"} padding=".25em">
            <Heading as="h4">{this.state.profile.name}</Heading>
            <Stats {...this.state.profile.powerstats} />
            <Appearance {...this.state.profile.appearance} />
            <Bio {...this.state.profile.biography} />
            <Work {...this.state.profile.work} />
            <Connections {...this.state.profile.connections} />
          </Block>
        </Grid.Item>
      )
    );
  };
}
export default CharaterProfile;
