import React, { Fragment } from "react";
import PropTypes from "prop-types";

import { List, Flex, Heading, styled } from "reakit";
import { isEmpty } from "../../utils/helpers";

const ConnectList = styled(List)`
  padding: 0.25em;
  li {
    text-transform: uppercase;
    text-transform: uppercase;
    font-weight: lighter;
    font-size: smaller;
  }
  li > b {
    text-transform: capitalize;
    padding-left: 0.65em;
    font-size: initial;
  }
`;

const Connections = ({ "group-affilations": affiliations, relatives }) =>(
    <Flex flexDirection="column">
      <Heading display="inline-block" as="h6">
        Connections
      </Heading>
      <ConnectList>
        {!isEmpty(affiliations) && (
          <li>
            affliations
            <b>{affiliations}</b>
          </li>
        )}
        {!isEmpty(relatives) && (
          <li>
            relatives
            <b>{relatives}</b>
          </li>
        )}
      </ConnectList>
    </Flex>
  );
export default Connections;
