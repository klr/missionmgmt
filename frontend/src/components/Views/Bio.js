import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { List, Flex, Block, Heading, styled, Inline } from "reakit";
import { isEmpty } from "../../utils/helpers";
const MetaList = styled(List)`
  padding: 0.25em;
  text-transform: uppercase;
  font-weight: lighter;
  font-size: smaller;
  li {
    text-transform: uppercase;
    font-weight: lighter;
    font-size: smaller;
  }
  li > b {
    text-transform: capitalize;
    padding-left: 0.65em;
    font-size: initial;
  }
`;
const AliasList = styled(List)`
  padding: 0.25em;
  text-transform: uppercase;
  font-weight: lighter;
  font-size: 11.1167px;
  li {
    text-transform: capitalize;
    padding-left: 0.65em;
    font-size: initial;
    text-indent:6ch;
  }
`;

const Bio = ({
  "full-Name": fullName,
  "alter-ego": altEgo,
  aliases,
  place_of_birth: poBirth,
  first_apperance: firstApperance,
  publisher,
  alignment
}) => (
  <Flex flexDirection="column">
    <Heading display="inline-block" as="h6">
      Biography
    </Heading>
    <MetaList>
      {!isEmpty(fullName) && (
        <li>
          full name
          <b>{fullName}</b>
        </li>
      )}
      {!isEmpty(altEgo) && (
        <li>
          alter ego
          <b>{altEgo}</b>
        </li>
      )}
      {!isEmpty(poBirth) && (
        <li>
          place of birth
          <b>{poBirth}</b>
        </li>
      )}
      {!isEmpty(firstApperance) && (
        <li>
          first apperance
          <b>{firstApperance}</b>
        </li>
      )}
      {!isEmpty(publisher) && (
        <li>
          publisher
          <b>{publisher}</b>
        </li>
      )}
      {!isEmpty(alignment) && (
        <li>
          alignment
          <b>{alignment}</b>
        </li>
      )}
    </MetaList>
    {!isEmpty(aliases) && (
      <AliasList>
        Aliases
        {Array.isArray(aliases) ? aliases.map(alias => <li><b>{alias}</b></li>) : <li>{aliases} </li>}
      </AliasList>
    )}
  </Flex>
);
export default Bio;
