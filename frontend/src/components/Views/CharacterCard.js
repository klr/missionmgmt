import React, { PureComponent } from "react";
import { Button, Card, Heading, Image, styled } from "reakit";

class CharacterCard extends PureComponent {
  render = () => {
    const { id, name, image, alignment, submit } = this.props;

    const CharacterType = styled(Card)`
      background-color: ${props => (props.alignment === "good" ? " rgba(131, 219, 255,1)" : "rgba(218, 155, 112,1)")};
      margin: 0.23em;
    `;
    const CharacterImage = styled(Image)`
      max-width: 12em;
      max-height: 16em;
      object-fit: contain;
    `;
    return (
      <CharacterType alignment={alignment}>
        <CharacterImage src={image} />
        <Heading as="h4"> {name}</Heading>
        <Button width="85%" onClick={() => submit(id, alignment)}>
          Select
        </Button>
      </CharacterType>
    );
  };
}
export default CharacterCard;
