import React, { PureComponent } from "react";
import { Block, Button, Field, Flex, Group, Heading, InlineFlex, Input, Label, Navigation, Grid } from "reakit";
import ServiceLayer from "../../Service/Backend";
import CharacterCard from "../Views/CharacterCard";
import CharacterProfile from "../Views/CharacterProfile";

class MainLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: "", results: null, hero: null, villain: null, heroDetail: null, villianDetail: null };
  }
  handleClear = () =>
    this.setState({ value: "", results: null, hero: null, villain: null, heroDetail: null, villianDetail: null });

  handleChange = ({ target: { value } }) => this.setState(() => ({ value }));
  handleSubmit = evt => {
    console.debug(`${this.state.value} being searched for`);
    evt.preventDefault();
    ServiceLayer.getHeroByName(this.state.value).then(results => this.setState(() => ({ results })));
  };
  handleChacter = (id, alignment) =>
    alignment === "good" ? this.setState(() => ({ hero: id })) : this.setState(() => ({ villain: id }));

  render = () => {
    const isVillains =
      this.state.results && this.state.results.filter(character => character.alignment !== "good").length > 0;
    const isHeros =
      this.state.results && this.state.results.filter(character => character.alignment === "good").length > 0;
    const isCompareReady = this.state.hero && this.state.villain;
    return (
      <React.Fragment>
        <Navigation
          as={InlineFlex}
          static
          fixed
          flexDirection="row"
          alignItems="center"
          justifyContent="space-between"
          backgroundColor="grey"
          width="100%"
        >
          <Heading as="h2" width="50%" padding=".5em">
            Mission Mangagement System
          </Heading>
          {isCompareReady && (
            <Block width="25%">
              <Button onClick={this.handleClear}>Reset</Button>
            </Block>
          )}
          <Block as="form" width="25%" padding=".5em" onSubmit={this.handleSubmit}>
            <Field>
              <Label htmlFor="search" fontWeight="lighter" color="white">
                Search for Hero/Villain
              </Label>
              <Group>
                <Input
                  id="search"
                  name="name"
                  type="text"
                  placeholder="Nick Fury"
                  aria-label="name"
                  value={this.state.value}
                  onChange={this.handleChange}
                />
                <Button width="15%" padding="0 .25em" as={Input} type="submit" value="Search" />
              </Group>
            </Field>
          </Block>
        </Navigation>
        {!isCompareReady ? (
          <Flex>
            {this.state.results && (
              <React.Fragment>
                {isHeros && (
                  <Block width={isVillains ? "50%" : "100%"}>
                    {this.state.results.filter(character => character.alignment === "good").map((character, key) => (
                      <CharacterCard key={`${character.id}_${key}`} {...character} submit={this.handleChacter} />
                    ))}
                  </Block>
                )}
                {isVillains && (
                  <Block width={isHeros ? "50%" : "100%"}>
                    {this.state.results.filter(character => character.alignment !== "good").map((character, key) => (
                      <CharacterCard key={`${character.id}_${key}`} {...character} submit={this.handleChacter} />
                    ))}
                  </Block>
                )}
              </React.Fragment>
            )}
          </Flex>
        ) : (
          <Grid columns="repeat(2, 1fr)" autoRows="auto" gap="3vw">
            <CharacterProfile id={this.state.hero} />
            <CharacterProfile id={this.state.villain} />
          </Grid>
        )}
      </React.Fragment>
    );
  };
}
export default MainLayout;
