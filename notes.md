# Mission Management System Notes

My intention was to create a interactive site that would blend compartive analysis with the "Choose a Fighter" UI seen in arcade video games.

 

### Areas needed improvement
- testing, would have implemented AVA for backend testing and Jest/Enzyme for frontend. For backend would have chect for chaching/loadbottlenecks. For fronted would have done an intial unit test along with an intergration test to ensure expected functionality.
- Backend - would have preffered to ask question about the infrustructer need over creating a handshake system. Potentialy migrate the data once collected into either a cache or document db based system for better granularity control.
- Frontend - word have moved a lot of the actions and api calls to a redux middleware and aded more microinterations for state efects (retriving, completed, success, fail), show the actual choice for hero/villian and the option to add/remove each without dropping the counterpart view.